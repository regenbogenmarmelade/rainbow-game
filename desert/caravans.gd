extends Node2D

var questClear = false

func selectDialog() :
	
	if questClear :
		$AcerIsThereDialog.show()
		return

	if (!$Quest.is_accepted()) :
		$FirstDialog.show()
	else :
		$HelpMeDialog.show()
		

func growGrasses() :
	
	var player = Global.find_player()
	var acerGreen = get_node("/root/Main/Woods/AcerGreen/Follower")
	var acerGreenFollowYou = player.followers.has(acerGreen)
		
	if acerGreenFollowYou:
		if(!$Quest.is_accepted()) :
			$Quest.accept()
			
		$GrowDialog.show()	
		get_node("/root/Main/Woods/AcerGreen").findArea()
		$Quest.set_description("Acer wants to be by the caravan driver and the camels. Now the camels never have to go hungry again.")
		questClear = true
		return
	
	if Global.find_player().has_follower(Global.AuraType.WOODS):
		$OhNoDialog2.show()
				
		
