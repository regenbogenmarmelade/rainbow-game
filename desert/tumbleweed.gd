extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var rotation_speed = 5
export var translation_speed = 3;
var direction = 1
#export var translation_speed = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	
	if direction == 1:
		rotation_degrees += rotation_speed
		position.x = position.x + translation_speed
		
		if position.x > 3000:
			direction = -1
		
	
	elif direction == -1 :
		position.x = position.x - translation_speed
		rotation_degrees -= rotation_speed
		if position.x < -200:
			direction = 1
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
