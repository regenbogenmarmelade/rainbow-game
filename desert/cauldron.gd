extends Sprite

func _ready():
	pass
	
func _process(delta):
	# Yeah, this is inefficient. Fix later (TM)?
	if Questlog.all_ingredients_collected():
		$"../CauldronCommunicationArea".show()

# This method wins the game.
func cook_jam():
	# Win now!
	Questlog.win_win_win() # <-- Win here
	# You have won.
