extends Node2D

export var has_quumcuencholitas = false

func apply_litmus_test():
	if has_quumcuencholitas:
		if Questlog.has_ingredient(Global.AuraType.DESERT):
			$EnoughQuumcuencholitasDialog.show()
		else:
			$PlentyQuumcuencholitasDialog.show()
	else:
		$NoQuumcuencholitasDialog.show()

func like_the_carpet():
	Questlog.collect_ingredient(Global.AuraType.DESERT)
