extends StaticBody2D

var questClear = false
var waterOfLIfe = false

func _process(delta):
	if Global.find_player().has_follower(Global.AuraType.WOODS):
		$Leaf.show()

func setWaterOfLife() :
	waterOfLIfe = true

func selectDialog() :
	
	if questClear :
		$QuestClearDialog.show()
		return
	
	if $Quest.is_accepted() :
		if waterOfLIfe :
			$GiveDialog.show()
		else :	
			$QuestDialog.show()	
	
	else :
		$FirstDialog.show()	
		
		
func selectDialogTree() :
	if(waterOfLIfe) :
		$TreeOfSpiritDialog.show()
	else :
		$WitheredDialog.show()			
		
		

func giveWaterOfLife() :
	$live.show()
	questClear = true
	$QuestClearDialog.show()
	updateText()


func updateText() :
	var text = "Bring the Water of Life to the Beechleaf in the desert."
	
	if (waterOfLIfe) :
		text = text + "\n You have canteen of water."
	
	if(questClear) :	
		text = text + "\n You water the Tree of Sprit. Now it's growing!"
		
	$Quest.set_description(text)
		
		
