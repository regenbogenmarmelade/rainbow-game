extends StaticBody2D

var hasDanceOfSand = false
var hasDanceOfFlower = false
var hasAncientDance = false
var hasFishDance = false

func dancing() :
	
	$PlayerDancing.global_position = Global.find_player().global_position
	
	$PlayerDancing.visible = true
	Global.find_player().visible = false
	game.pause(Global.find_player())

	var t = Timer.new()
	t.set_wait_time(4)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")	
	t.queue_free()
	
	hasDanceOfSand = true
	
	$NiceDialog.show()
	$PlayerDancing.visible = false
	Global.find_player().visible = true
	game.resume(Global.find_player())
	updateDancceQuestMessage()
	
func updateDancceQuestMessage():
	
	var text = "You must learn how to [tornado]waltz[/tornado]. That will be a multistep process."
	
	if hasDanceOfSand :
		text = text + "\n- You learn the Dance of Sand"
		
	if hasDanceOfFlower :
		text = text + "\n- You learn the Dance of Flower"
		
	if hasFishDance :
		text = text + "\n- You learn the Dance of beached Fish"	
		
	if hasAncientDance :
		text = (text + "\n- You learn an Ancient Dance")
		
	$DanceQuest.set_description(text)			

func sethasDanceOfFlower () :
	hasDanceOfFlower = true
