extends Node

var music_volume = 0
var game_started : bool = false

enum AuraType {
	DESERT = 0,
	RUINS = 10,
	SWAMP = 20,
	VOLCANO = 30,
	WATER = 40,
	WOODS = 50
}

func find_aura_type_name(aura_type):
	for name in AuraType.keys():
		if AuraType.get(name) == aura_type:
			return name
	return null

func find_player() -> Player:
	return get_node("/root/Main/Player") as Player

func set_music_volume(volume):
	music_volume = volume
	var master_channel = 0
	AudioServer.set_bus_volume_db(master_channel, volume)

func _process(delta):
	if Input.is_action_just_pressed("mute"):
		var master_channel = 0
		var should_mute = !AudioServer.is_bus_mute(master_channel)
		AudioServer.set_bus_mute(master_channel, should_mute)
