extends ColorRect


var translation_speed = 5;
var startRuins = false

func _process(delta):
	if(startRuins) :
		$Dancer.position.x = $Dancer.position.x - translation_speed
		$Name.margin_left =  $Name.margin_left - translation_speed
		
		$Dodo.position.x = $Dodo.position.x - translation_speed
		$Dodo2.position.x = $Dodo2.position.x - translation_speed
		$Player.position.x = $Player.position.x - translation_speed
		$Name2.margin_left =  $Name2.margin_left - translation_speed
