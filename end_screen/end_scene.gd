extends CanvasLayer


func _process(delta):
	var t = Timer.new()
	t.set_wait_time(10)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")	
	t.queue_free()
	
	$Desert.show()
	$Credits.show()
	
	if($Desert.visible):
		$Desert.startDesert = true
	
	
	if $Desert/Tumbleweed.position.x < -200 :
		$Woods.show()
		$Woods.startWoods = true
		
	if $Woods/Mushroom.position.x < -200 :
		$Vulcano.show()
		$Vulcano.startVulcano = true

	if $Vulcano/Vulcan.position.x < -200 :
		$Water.show()
		$Water.startWater = true
		
	if $Water/Fish.position.x < -200 :
		$Swamp.show()
		$Swamp.startSwamp = true	
		
	if $Swamp/Frog.position.x < -200 :
		$Ruins.show()
		$Ruins.startRuins = true		
		
	if $Ruins/Dodo.position.x < -200 :
		$Credits.hide()
		$Endscene.show()
		$Endscene.startEndSceen = true			
	
