extends ColorRect

var startEndSceen = false
var modulationSepia = 0
var modulationNormal = 1
var modSpeed = 0.005


func _process(delta):
	
	var t = Timer.new()
	t.set_wait_time(2)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")	
	t.queue_free()
	
	if (startEndSceen) :
		if(modulationSepia < 1) :
			modulationSepia = modulationSepia + modSpeed
			$Sprite2.modulate.a = modulationSepia
			
			$ThankYou.modulate.a = modulationSepia
	
