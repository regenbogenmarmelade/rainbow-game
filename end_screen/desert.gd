extends ColorRect

var translation_speed = 5;
var startDesert = false

var tumbleSpeed = 5
var tumbleRotation = 5

func _process(delta):
	if(startDesert) :
		$Dancer.position.x = $Dancer.position.x - translation_speed
		$Name.margin_left =  $Name.margin_left - translation_speed
		$PlayerDance.position.x = $PlayerDance.position.x - translation_speed
		
		$Tumbleweed.rotation_degrees -= tumbleRotation
		$Tumbleweed.position.x = $Tumbleweed.position.x - tumbleSpeed
		
		$Tumbleweed2.rotation_degrees -= tumbleRotation
		$Tumbleweed2.position.x = $Tumbleweed2.position.x - tumbleSpeed
		$Name2.margin_left =  $Name2.margin_left - translation_speed
				
	
