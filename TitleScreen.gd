extends Control

var current_scene
var loader
var scene_loaded : bool = false
var resource

func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() -1)

func _process(time):
	if loader == null:
		set_process(false)
		return
	
	var err = loader.poll()
	if err == ERR_FILE_EOF:
		resource = loader.get_resource()
		loader = null
		scene_loaded = true
		get_ready()
		
	elif err == OK:
		pass
	else:
		loader = null

func _input(event):
	if event.is_action_released("ui_accept"):
		start_game()

func start_game():
	if !scene_loaded:
		return

	set_new_scene(resource)
	set_process_input(false)
	queue_free()

func load_game():
	if loader != null:
		return

	loader = ResourceLoader.load_interactive("res://main.tscn")
	if loader == null: 
		return
	set_process(true)
	
	$Loading.visible = true
	$Buttons.visible = false
	
func get_ready():
	$Loading/LoadingShrooms.moving = true
	$Loading/LoadingText.visible = false
	$Loading/ReadyText.visible = true
	
func set_new_scene(scene_resource):
	current_scene = scene_resource.instance()
	get_node("/root").add_child(current_scene)

func _on_HSlider_value_changed(volume):
	Global.set_music_volume(volume)
