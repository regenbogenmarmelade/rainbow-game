extends Node2D

func repair():
	var player = Global.find_player()
	var brokenist = get_node("/root/Main/Ruins/Brokenist")
	if !player.followers.has(brokenist.get_node("TimeGuy/Follower")):
		return

	var has_hammer = player.has_follower(Global.AuraType.VOLCANO)
	if !has_hammer:
		return

	var dialog = brokenist.get_node("StopDialog")
	dialog.show()
