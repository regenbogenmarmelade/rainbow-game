extends YSort

func _ready():
	setTimeGuyVisibility(false)

func setTimeGuyVisibility(visibilityBool) :
	var main_scene = get_node("/root/Main")
	var all_timeguys = get_tree().get_nodes_in_group("timeguys")
	for node in all_timeguys:
		var is_in_ruins = main_scene.get_path_to(node).get_name(0) == name
		if !is_in_ruins:
			continue
		node.visible = visibilityBool
		
		var toggle_collisions = funcref(node, "toggle_collisions")
		if toggle_collisions.is_valid():
			toggle_collisions.call_func(visibilityBool)

	$Brokenist/TimeGuy.visible = true
	$Brokenist/TimeGuy/Aura.visible = visibilityBool
	$Brokenist/TimeGuy.toggle_collisions(!visibilityBool)
