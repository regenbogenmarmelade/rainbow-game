extends Node2D

func start_following():
	$TimeGuy.toggle_collisions(false)

	Global.find_player().add_follower($TimeGuy/Follower)
	$Quest.accept()
	get_node("/root/Main/Ruins/Bridge/BridgeHole").queue_free()

	var target_area = get_node("/root/Main/Ruins/Bridge/BridgistTarget")
	target_area.connect("player_entered", self, "reach_target", [target_area])

func reach_target(target_area : Node2D):
	Global.find_player().remove_follower($TimeGuy/Follower)
	$TimeGuy.global_position = target_area.global_position

	$TimeGuy.toggle_collisions(true)
	
	$Quest.description = \
"""
You brought the bridgist to the bridge.
The bridgist is very thankful.
"""

	$Quest.completed = true
	target_area.queue_free()
	$ThankYou.show()


func talk():
	if $Quest.completed:
		$AfterQuestDialog.show()
	else:
		$Dialog.show()
