extends StaticBody2D

signal talk

func forward_talk_signal():
	emit_signal("talk")

func toggle_collisions(enabled):
	$CollisionShape2D.set_deferred("disabled", !enabled)
	$CommunicationArea.set_enabled(enabled)
