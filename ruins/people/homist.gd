extends Node2D



func startDialog() :
	
	var ivyQuestClear = get_node("//root/Main/Woods/Ivy").questClear
	if(ivyQuestClear) :
		$FindTreasureDialog.show()
		return
	
	var ivyQuest = get_node("//root/Main/Woods/Ivy/Quest")
	if(ivyQuest.is_accepted()) :
		$TranslateDialog.show()
		ivyQuest.set_description("The ancient writings said, there was a forgotten island to the east and there was a tresure.")	
		
		var ivy = get_node("//root/Main/Woods/Ivy")
		ivy.setMeet()
		

	
