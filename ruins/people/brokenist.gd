extends Node2D

func check_hammer_availability():
	var player = Global.find_player()
	var has_hammer = player.has_follower(Global.AuraType.VOLCANO)
	if has_hammer:
		$HasHammerDialog.show()
	else:
		$NoHammerDialog.show()

func repair_the_clocktower():
	game.pause(self)
	$RepairSound.play()

func receive_berries():
	Questlog.collect_ingredient(Global.AuraType.RUINS)
	start_following()

func start_following():
	var player = Global.find_player()
	if player.has_follower(Global.AuraType.RUINS):
		return

	player.add_follower($TimeGuy/Follower)
	$TimeGuy.toggle_collisions(false)

func repair_finished():
	var ruins = get_node("/root/Main/Ruins")
	ruins.setTimeGuyVisibility(true)
	
	$ArrivalSound.play()
	ruins.get_node("Clocktower/TriggerArea").queue_free()

func arrival():
	$FixedClocktowerDialog.show()
	game.resume(self)
