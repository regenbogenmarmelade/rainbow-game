extends Node2D

var foundDodo1 = false
var foundDodo2 = false
var foundDodo3 = false
var foundDodo4 = false
var foundDodo5 = false

var numFoundDodos = 0

func updateNumOfDodos() :
	numFoundDodos = int(foundDodo1) + int(foundDodo2) + int(foundDodo3) + int(foundDodo4) + int(foundDodo5)
	
	match numFoundDodos :
		1: 
			$Quest.set_description("The Dodoist lost all his Dodos. You found 1/5 Dodos.")
		2: 
			$Quest.set_description("The Dodoist lost all his Dodos. You found 2/5 Dodos.")
		3:
			$Quest.set_description("The Dodoist lost all his Dodos. You found 3/5 Dodos.")
		4:
			$Quest.set_description("The Dodoist lost all his Dodos. You found 4/5 Dodos.")
		5:
			$Quest.set_description("You catch all (5/5) Dodos! Yeah!")

	

func selectDialogs() :
	
	#numFoundDodos = int(foundDodo1) + int(foundDodo2) + int(foundDodo3) + int(foundDodo4) + int(foundDodo5)
	
	if !$Quest.is_accepted() :
		$Dialog.show()
		return
	
	match numFoundDodos :
		0:
			$FoundNoDodosDialog.show()
		1: 
			$FoundOneDodoDialog.show()
			#$Quest.set_description("The Dodoist lost all his Dodos. You found 1/5 Dodos.")
		2: 
			$FoundTwoDodosDialog.show()
			#$Quest.set_description("The Dodoist lost all his Dodos. You found 2/5 Dodos.")
		3:
			$FoundThreeDodosDialog.show()
			#$Quest.set_description("The Dodoist lost all his Dodos. You found 3/5 Dodos.")
		4:
			$FoundFourDodosDialog.show()
			#$Quest.set_description("The Dodoist lost all his Dodos. You found 4/5 Dodos.")
		5:
			$FoundAllDodosDialog.show()	
			#$Quest.set_description("You catch all (5/5) Dodos! Yeah!")
	
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
