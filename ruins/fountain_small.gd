extends StaticBody2D


func selectDialog() :
	
	var treeOfSpiritQuest = get_node("/root/Main/Desert/TreeOfSpirit/Quest") 
	
	if Global.find_player().has_follower(Global.AuraType.WATER):
		if treeOfSpiritQuest.is_accepted() :  
			$DialogWithWaterWithQuest.show()
		else :
			$DialogWithWaterWithoutQuest.show()
	
	else :
		$DialogNoWater.show()		
				
		

func hasWaterOfLife() :
	
	var treeOfSpirit = get_node("/root/Main/Desert/TreeOfSpirit")
	treeOfSpirit.setWaterOfLife()
	treeOfSpirit.updateText()
	
	
