extends StaticBody2D

var hasMedicine = false

func has_medicine() : 
	var witch = get_node("/root/Main/Woods/Witch")
	
	if Global.find_player().has_follower(Global.AuraType.RUINS):
		if witch.hasMedicine:
			$ThankYouDialog.show()
			$Quest.set_description("You give the soldier medicine.")
		else:
			$FirstDialog.show()
			
		return	
		
	$WindDialog.show()
	
	
