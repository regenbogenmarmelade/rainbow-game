extends StaticBody2D

func dancing() :
	
	$PlayerDancing.global_position = Global.find_player().global_position
	$PlayerDancing.visible = true
	Global.find_player().visible = false
	game.pause(Global.find_player())

		
	var t = Timer.new()
	t.set_wait_time(4)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")	
	t.queue_free()
	
	var dancer = get_node("/root/Main/Desert/YSort/EntertainmentGroup/Dancer")
	dancer.hasAncientDance = true
	$PlayerDancing.visible = false
	Global.find_player().visible = true
	game.resume(Global.find_player())
	
	dancer.updateDancceQuestMessage()
