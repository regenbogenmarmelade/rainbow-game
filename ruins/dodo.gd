extends Node2D

export var translation_speed = 1;
var direction = 1

var iAmFound = false
var inEnclosure = false


func _process(delta):
	
	if !iAmFound :
		
		if direction == 1:
			position.x = position.x + translation_speed		
			if position.x > 1500:
				direction = -1
				$DodoStaticBody/AnimatedSprite.flip_h = false
	
		elif direction == -1 :
			position.x = position.x - translation_speed
	
			if position.x < -50:
				direction = 1
				$DodoStaticBody/AnimatedSprite.flip_h = true

	else :
		if direction == 1:
			position.x = position.x + translation_speed		
			if position.x > 3100:
				direction = -1
				$DodoStaticBody/AnimatedSprite.flip_h = false
	
		elif direction == -1 :
			position.x = position.x - translation_speed
	
			if position.x < 2900:
				direction = 1
				$DodoStaticBody/AnimatedSprite.flip_h = true
		

func wasFound() :
	translation_speed = 0
	$DodoStaticBody/AnimatedSprite.stop()
	
	if get_node("/root/Main/Ruins/Dodoist/Quest").is_accepted() && !inEnclosure :
		$QuestDialog.show()
		iAmFound = true
		
	else :
		$Dialog.show()	
	
	
func sentToDodoEnclosure() :
	global_position = get_node("/root/Main/Ruins/DodoEnclosure/DodoPosition").global_position
	get_node("/root/Main/Ruins/Dodoist").foundDodo1 = true
	get_node("/root/Main/Ruins/Dodoist").updateNumOfDodos()
	inEnclosure = true
	
func endTalk() :
	translation_speed = 1
	$DodoStaticBody/AnimatedSprite.play()

