extends StaticBody2D

func wasFound() :
	if get_node("/root/Main/Ruins/Dodoist/Quest").is_accepted() :
		$QuestDialog.show()
		
	else :
		$Dialog.show()	
	
	
func sentToDodoEnclosure() :
	global_position = get_node("/root/Main/Ruins/DodoEnclosure/DodoPosition3").global_position
	get_node("/root/Main/Ruins/Dodoist").foundDodo3 = true
	get_node("/root/Main/Ruins/Dodoist").updateNumOfDodos()
