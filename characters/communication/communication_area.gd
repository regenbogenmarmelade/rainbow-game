tool
extends Node2D

export var area_scale = Vector2(1,1)

signal talk

func _process(delta):
	if scale != Vector2(1, 1):
		push_error("Please don't scale me, I have an area scale property")
		assert(false)
	
	$TriggerArea.scale = area_scale

func speech_bubble_pressed():
	if Global.find_player().get_paused():
		return
	emit_signal("talk")

func set_enabled(enabled):
	$TriggerArea/CollisionShape2D.set_deferred("disabled", !enabled)
