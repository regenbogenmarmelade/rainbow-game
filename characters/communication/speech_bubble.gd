extends TextureButton

const group_name = "speech_bubbles"

func _ready():
	hide()

func visibility_changed():
	if visible:
		add_to_group(group_name)
		if !Global.find_player().get_paused():
			grab_focus()
	else:
		if has_focus() && focus_next != null:
			get_node(focus_next).grab_focus()
		
		if get_groups().has(group_name):
			remove_from_group(group_name)
	
	get_tree().call_group(group_name, "update_focus_neighbors")

func update_focus_neighbors():
	var speech_bubbles = get_tree().get_nodes_in_group(group_name)
	var index = speech_bubbles.find(self)
	if index == len(speech_bubbles) - 1:
		focus_next = speech_bubbles[0].get_path()
	else:
		focus_next = speech_bubbles[index + 1].get_path()
