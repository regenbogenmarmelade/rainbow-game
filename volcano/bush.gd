extends Sprite

func harvest():
	var player = Global.find_player()
	var is_volcano_behind_you = player.has_follower(Global.AuraType.VOLCANO)
	if !is_volcano_behind_you:
		return
	$TriggerArea.queue_free()
	$Sprite.queue_free()
	var volcano = get_parent()
	volcano.collect_chilis()
