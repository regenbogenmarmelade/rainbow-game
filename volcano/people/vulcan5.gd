extends Node2D

func talk():
	var player = Global.find_player()
	var brokenist = get_node("/root/Main/Ruins/Brokenist/TimeGuy/Follower")
	var knows_about_broken_clocktower = player.followers.has(brokenist)
	if knows_about_broken_clocktower:
		$RepairClocktowerDialog.show()
	else:
		$MyHammerDialog.show()

func lets_go():
	$CommunicationArea.set_enabled(false)
	$StaticBody2D/CollisionPolygon2D.disabled = true
	var player = Global.find_player()
	player.add_follower($Follower)
