extends YSort

var number_of_chili_bushes = 4
var number_of_maximum_sixths_fingerlengths_of_collected_chilis = 9.0
var sixths_fingerlengths_per_chili_bush = number_of_maximum_sixths_fingerlengths_of_collected_chilis / number_of_chili_bushes
var number_harvested_chili_bushes = 0

func collect_chilis():
	number_harvested_chili_bushes += 1
	var collected = number_harvested_chili_bushes * sixths_fingerlengths_per_chili_bush
	var remaining = number_of_maximum_sixths_fingerlengths_of_collected_chilis - collected
	if remaining < 1:
		Questlog.collect_ingredient(Global.AuraType.VOLCANO)
	else:
		var remaining_handful = remaining / 5.0
		var text = "You have collected " + str(collected) + "/6 fingerlengths of chili peppers.\n"
		text += "You still need " + str(remaining_handful) + "/6 handful."
		$ChiliDialog.dialog_text = text
		$ChiliDialog.show()
