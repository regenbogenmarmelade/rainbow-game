extends StaticBody2D

var boatIsFixed = false
var isOnForgottenIsland = false


func selectDialog() : 
	if (boatIsFixed) :
		if(!isOnForgottenIsland) :
			$ToEastDialog.show()
		else :
			$GoWestDialog.show()	
		return
	
	$BoatBrokenDialog.show()	
		

func repairBoat() :
	
	var player = Global.find_player()
	var repairMan = get_node("/root/Main/Volcano/vulcan5/Follower")
	var hasRepairMan = player.followers.has(repairMan)
	
	if hasRepairMan :
		$RepairManDialog.show()
		$BrokenBoat.visible = false
		$Boat.visible = true	
		
		boatIsFixed = true
		
		$Quest.set_description("You fixed Charons boat.")
	

func sayThankYou() :
	$RepairedBoatDialog.show()
	$Boat.visible = false
	$BoatCharon.visible = true
	$CharonOnly.visible = false
			
						

func goEast() : 
	#if (goToForgottenIslnd) :
		
	var player = Global.find_player()
	
	while player.global_position.x < $ForgottenIsland.global_position.x :
		player.global_position.x = player.global_position.x + 0.5
		$BoatCharon.global_position.x = $BoatCharon.global_position.x + 0.5
		$CharonAndPlayer.global_position.x = $CharonAndPlayer.global_position.x + 0.5
		$CommunicationArea.global_position.x = $CommunicationArea.global_position.x + 0.5
		
	isOnForgottenIsland = true	
	#$CharonAndPlayer.visible = false		

func goWest() :
	$BoatCharon.flip_h = true
	$CharonAndPlayer.flip_h = true
	$CharonAndPlayer.visible = true
	var player = Global.find_player()
	while player.position.x > $Beach.global_position.x :
		player.position.x = player.position.x - 0.5
		$BoatCharon.position.x = $BoatCharon.position.x - 0.5
		$CharonAndPlayer.global_position.x = $CharonAndPlayer.global_position.x - 0.5
		$CommunicationArea.position.x = $CommunicationArea.position.x - 0.5
		
		$BoatCharon.flip_h = true
		$CharonAndPlayer.flip_h = true
	
		
	isOnForgottenIsland = false
	$CharonAndPlayer.visible = false
	$BoatCharon.flip_h = false
	$CharonAndPlayer.flip_h = true
