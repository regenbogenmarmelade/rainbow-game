extends StaticBody2D

var inEnclosure = false

func wasFound() :
	
	if inEnclosure :
		$DialogEnclosure.show()
		return
	
	if get_node("/root/Main/Ruins/Dodoist/Quest").is_accepted() :
		$QuestDialog.show()
		
	else :
		$Dialog.show()	
	
	
func sentToDodoEnclosure() :
	global_position = get_node("/root/Main/Ruins/DodoEnclosure/DodoPosition4").global_position
	get_node("/root/Main/Ruins/Dodoist").foundDodo4 = true
	get_node("/root/Main/Ruins/Dodoist").updateNumOfDodos()
	inEnclosure = true
