extends StaticBody2D

func selectDialog() :
	
	var ivy = get_node("//root/Main/Woods/Ivy")
	var ivyQuest = get_node("//root/Main/Woods/Ivy/Quest")
	
	if(ivyQuest.is_accepted() && ivy.meetTimeGuy) :
		$DialogWith.show()
		ivyQuest.set_description("You found the greatest treasure: friends!")
		ivy.setQuestClear()
		
	else :
		$DialogWithout.show() 
