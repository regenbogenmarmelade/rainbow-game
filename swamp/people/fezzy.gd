extends AnimatedSprite

var dialog_index = null

func talk():
	if dialog_index == null:
		var swamp = get_parent()
		dialog_index = swamp.fezzy_dialog_index
		swamp.fezzy_dialog_index += 1
	
	get_node("Dialog" + str(dialog_index)).show()
