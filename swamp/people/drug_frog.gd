extends Node2D

func player_entered():
	var swamp = get_parent()
	var has_talked_to_all_the_fezzys = swamp.fezzy_dialog_index > 3

	if has_talked_to_all_the_fezzys:
		$TriggerArea.queue_free()
		Global.find_player().add_follower($Follower)
	else:
		$Dialog.show()
