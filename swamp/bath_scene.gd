extends Node2D

var frog_diving = false

func pervert_incoming(body):
	if frog_diving:
		return

	$Frog/Animations.play("hide")
	frog_diving = true

func pervert_gone(body):
	if frog_diving:
		$Frog/Animations.play("unhide")
		yield($Frog/Animations, "animation_finished")
		$Frog/Animations.play("wiggle")
		frog_diving = false

func surprise(body):
	if frog_diving:
		return

	$Frog/Animations.play("surprised")
	$SurpriseDialog.show()
	frog_diving = true
