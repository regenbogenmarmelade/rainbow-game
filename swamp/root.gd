extends Node2D

func player_entered():
	if Global.find_player().has_follower(Global.AuraType.SWAMP):
		$Dialog.show()

func collect_root():
	Questlog.collect_ingredient(Global.AuraType.SWAMP)
	queue_free()
