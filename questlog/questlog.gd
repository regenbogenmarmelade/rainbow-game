extends CanvasLayer

var open = false

func _input(event):
	if !Global.game_started:
		return
	if event.is_action_pressed("questlog"):
		toggle_visibility()
	if event.is_action_pressed("click"):
		if open:
			toggle_visibility()
		elif event.position.x < 300 and event.position.y < 200: # Screw this.
			toggle_visibility()
	if event.is_action_pressed("cheat"):
		cheat_next_ingredient()

func activate_questlog():
	# We now always want to see the popup! :D
	$PopupDialog.show()

func toggle_visibility():
	if open:
		$AnimationPlayer.play_backwards("open")
	else:
		game.pause(self)
		$AnimationPlayer.play("open")
	open = not open

func add_quest(new_quest):
	if has_quest(new_quest):
		return
		
	var quest_item = RichTextLabel.new()
	quest_item.bbcode_enabled = true
	quest_item.bbcode_text = new_quest.description
	quest_item.set_meta("quest_id", new_quest.get_instance_id())
	quest_item.rect_min_size.x = 400
	quest_item.rect_min_size.y = 100
	quest_item.size_flags_vertical = Control.SIZE_EXPAND_FILL
	$PopupDialog/VBoxContainer.add_child(quest_item)

func has_quest(quest):
	var quest_items = $PopupDialog/VBoxContainer.get_children()
	var quest_id = quest.get_instance_id()
	
	for item in quest_items:
		if item.get_meta("quest_id") == quest_id:
			return true
	
	return false

func update_quest(quest):
	var quest_items = $PopupDialog/VBoxContainer.get_children()
	var quest_id = quest.get_instance_id()
	
	var existing_item
	for item in quest_items:
		if item.get_meta("quest_id") == quest_id:
			existing_item = item
			break
	
	if existing_item == null:
		return
	
	existing_item.bbcode_text = quest.description

func collect_ingredient(aura_type):
	for node in $PopupDialog/Ingredients.get_children():
		var ingredient = node as Ingredient
		if ingredient.aura_type == aura_type:
			if not open:
				toggle_visibility()
				yield($AnimationPlayer, "animation_finished")
			ingredient.collect()
			if not open:
				toggle_visibility()
			break
	if all_ingredients_collected():
		win()

func has_ingredient(aura_type):
	for node in $PopupDialog/Ingredients.get_children():
		var ingredient = node as Ingredient
		if ingredient.aura_type == aura_type:
			return ingredient.is_collected

func animation_finished(anim_name):
	if !open:
		game.resume(self)

func cheat_next_ingredient():
	for node in $PopupDialog/Ingredients.get_children():
		var ingredient = node as Ingredient
		if not has_ingredient(ingredient.aura_type):
			collect_ingredient(ingredient.aura_type)
			return

func all_ingredients_collected():
	var all_collected = true
	for node in $PopupDialog/Ingredients.get_children():
		var ingredient = node as Ingredient
		if not has_ingredient(ingredient.aura_type):
			all_collected = false
	return all_collected

func win():
	$JamQuest.accept()

func win_win_win():
	#$WinScreen/AnimationPlayer.play("win_win_win")
	var endScene = preload("res://end_screen/end_scene.tscn").instance()
	var root = get_tree().get_root()
	root.add_child(endScene)
	
