class_name Ingredient
tool

extends Control

const AuraType = Global.AuraType
export(AuraType) var aura_type
export(String, MULTILINE) var label_text setget set_label_text, get_label_text
export(StreamTexture) var icon_resource setget set_icon_resource, get_icon_resource

var is_collected = false

func set_icon_resource(new_resource):
	$Sprite.texture = new_resource
	
func get_icon_resource():
	return $Sprite.texture

func set_label_text(new_text):
	$Label.bbcode_text = new_text
	
func get_label_text():
	return $Label.bbcode_text

func collect():
	is_collected = true

	var tween = Tween.new()
	add_child(tween)
	tween.interpolate_property($Sprite.material, "shader_param/colorfulness", 0, 1, 1)
	tween.interpolate_property($Label, "self_modulate", Color(1, 1, 1, 0.5), Color(1, 1, 1, 1), 1)
	tween.start()
	#yield(tween, "tween_all_completed")
	var explosion = preload("res://player/heart_explosion.tscn").instance()
	add_child(explosion)
