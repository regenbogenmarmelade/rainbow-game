extends Node2D

export(String, MULTILINE) var description setget set_description, get_description

var completed = false

func accept():
	Questlog.add_quest(self)

func is_accepted():
	return Questlog.has_quest(self)

func get_description():
	return description

func set_description(new_description):
	description = new_description
	if Questlog:
		# This if is a hack so that the questlog can contain a quest itself...
		Questlog.update_quest(self)
