extends KinematicBody2D
class_name Player

# Player movement speed
export var speed: int

export var love : int = 3 setget set_love, get_love

export var paused : bool = false setget set_paused, get_paused

var followers = []

func _input(event):
	if paused == false:
		if event.is_action_pressed("love"):
			if love > 0:
				var explosion = preload("res://player/heart_explosion.tscn").instance()
				explosion.position = position + Vector2(0, -50)
				get_parent().add_child(explosion)
				call_deferred("love_gone")

func _physics_process(delta):
	var direction: Vector2
	direction.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	direction.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	if paused == false:
		# Get player input
		if Input.is_action_pressed("click"):
			var mouse_pos = get_global_mouse_position()
			direction = position.direction_to(mouse_pos)
	
		if abs(direction.x) == 1 and abs(direction.y) == 1:
			direction = direction.normalized()
		
		var speed_multiplier = 2
		if Input.is_action_pressed("sprint"):
			speed_multiplier = 4
	
		var movement = speed * speed_multiplier * direction * delta
		move_and_slide(movement)
	
		# Animate player based on direction
		animates_player(direction)
		
		$FootStepRotator.rotation = direction.angle() + PI/2.0
	else:
		animates_player(direction)

func animates_player(direction: Vector2):
	if direction.length() == 0 || paused == true:
		$AnimationTree.get("parameters/playback").travel("idle")
		if $Steps.playing:
			$Steps.stop()
		$FootStepRotator/FootSteps.emitting = false
	else:
		$AnimationTree.get("parameters/playback").travel("walk")
		$AnimationTree.set("parameters/walk/blend_position", direction)
		$AnimationTree.set("parameters/idle/blend_position", direction)
		if not $Steps.playing:
			$Steps.play()
		$FootStepRotator/FootSteps.emitting = true


func add_follower(new_follower : Follower):
	var leader
	if followers.empty():
		leader = self
	else:
		leader = followers.back().get_parent()

	new_follower.start_following(leader)
	followers.append(new_follower)

func remove_follower(old_follower : Follower):
	old_follower.stop_following()
	
	if old_follower == followers.back():
		followers.pop_back()
		return
	
	if old_follower == followers.front():
		var next_follower = followers[1] as Follower
		next_follower.start_following(self)
		followers.pop_front()
		return
	
	var follower_index = followers.find(old_follower)
	var previous_follower = followers[follower_index - 1] as Follower
	var next_follower = followers[follower_index + 1] as Follower
	next_follower.start_following(previous_follower.get_parent())
	followers.remove(follower_index)

func has_follower(aura_type):
	for node in followers:
		var aura = (node as Node).get_parent().get_node("Aura")
		if aura.aura_type == aura_type:
			return true

	return false

func _on_Love_Timer_timeout():
	if love < 3:
		love +=1

func get_love():
	return love
	
func set_love(new_love):
	love = new_love
	
func love_gone():
	love -= 1
	
func set_paused(pause : bool):
	paused = pause
	
func get_paused():
	return paused
