extends StaticBody2D

var questClear = false
var meetTimeGuy = false

func follow():
	toggle_collisions(false)
	Global.find_player().add_follower($Follower)


func toggle_collisions(enabled):
	$CollisionShape2D.set_deferred("disabled", !enabled)
	$CommunicationArea.set_enabled(enabled)

func setQuestClear() :
	questClear = true

func setMeet() :
	meetTimeGuy = true
