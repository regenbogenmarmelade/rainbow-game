extends StaticBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var translation_speed = 3;
var direction = 1
#export var translation_speed = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	if $Dialog1/PopupDialog.visible || $Dialog2/PopupDialog.visible:
		return
	if direction == 1:
		position.x = position.x + translation_speed
		$AnimatedSprite.play("right")

		if position.x > 4000:
			direction = -1
		
	elif direction == -1 :
		position.x = position.x - translation_speed
		$AnimatedSprite.play("left")
		
		if position.x < 1000:
			direction = 1
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
