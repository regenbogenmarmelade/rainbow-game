extends Node2D

func show_harvest_dialog():
	var woods = get_parent()
	var guide = woods.get_node("AcerBrown")
	var player = Global.find_player()
	if !player.followers.has(guide.get_node("Follower")):
		return

	guide.get_node("Timer").stop()
	$Dialog.show()

func harvest():
	Questlog.collect_ingredient(Global.AuraType.WOODS)
	queue_free()
