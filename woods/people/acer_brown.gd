extends Node2D

func follow():
	$StaticBody2D.queue_free()
	$CommunicationArea.queue_free()
	Global.find_player().add_follower($Follower)
	$Timer.start()

func get_direction_text():
	var target = get_parent().get_node("Tree17") as Node2D
	var player = Global.find_player()
	
	var distance = target.global_position - player.global_position
	
	if distance.length() < 800:
		return "The spice plant is not far from here."
	
	if abs(distance.x) > 300:
		if distance.x > 0:
			return "The spice plant is east of here."
		if distance.x < 0:
			return "The spice plant is west of here."

	if distance.y > 0:
		return "The spice plant is south of here."
	if distance.y < 0:
		return "The spice plant is north of here."

func timeout():
	if Questlog.has_ingredient(Global.AuraType.WOODS):
		$Timer.stop()
		return

	$DirectionDialog.dialog_text = get_direction_text()
	$DirectionDialog.show()
