extends StaticBody2D

var isInDesert = false

func selectDialog() :
	if(isInDesert) :
		$IsInDesertDialog.show()
		return
		
	$Dialog.show()
	

func follow():
	toggle_collisions(false)
	Global.find_player().add_follower($Follower)


func toggle_collisions(enabled):
	$CollisionShape2D.set_deferred("disabled", !enabled)
	$CommunicationArea.set_enabled(enabled)
	

func findArea() : 	
	Global.find_player().remove_follower($Follower)
	
	var targetArea = get_node("//root/Main/Desert/Caravans/TriggerArea")
	global_position = targetArea.global_position

	toggle_collisions(true)
	targetArea.queue_free()
	isInDesert = true
	
