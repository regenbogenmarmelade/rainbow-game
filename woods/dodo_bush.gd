extends StaticBody2D

export var translation_speed = 3;
var direction = 1

var iAmFound = false
var isActive = false
var isCatched = false


func _process(delta):
	
	if !iAmFound && isActive:
		
		$DodoCommunicationArea.set_block_signals(false)
		$DodoCommunicationArea.visible = true
		
		if direction == 1:
			position.x = position.x + translation_speed		
			if position.x > -150:
				direction = -1
				$AnimatedSprite.flip_h = false
	
		elif direction == -1 :
			position.x = position.x - translation_speed
	
			if position.x < -1200:
				direction = 1
				$AnimatedSprite.flip_h = true

	elif(iAmFound && isActive) :
		if direction == 1:
			position.x = position.x + translation_speed		
			if position.x > 1000:
				direction = -1
				$AnimatedSprite.flip_h = false
	
		elif direction == -1 :
			position.x = position.x - translation_speed
	
			if position.x < 3000:
				direction = 1
				$AnimatedSprite.flip_h = true
	
	else:
		visible = false
		$DodoCommunicationArea.set_block_signals(true)
		$DodoCommunicationArea.visible = false	

func wasFound() :
	translation_speed = 0
	$AnimatedSprite.stop()
	
	if isCatched :
		$InEnclosureDialog.show()
		return
	
	if get_node("/root/Main/Ruins/Dodoist/Quest").is_accepted() :
		$DodoDialog.show()
		iAmFound = true
		
	else :
		$DialogLeave.show()	
	
func catchDodo():
	var randomNoGenerator = RandomNumberGenerator.new()
	randomNoGenerator.randomize()
	var randomNo = randomNoGenerator.randi_range (0,1)
	
	if bool(randomNo):
		$CatchDodoDialog.show()
	else :
		$TryAgainDialog.show()
			
	
func sentToDodoEnclosure() :
	global_position = get_node("/root/Main/Ruins/DodoEnclosure/DodoPosition5").global_position
	get_node("/root/Main/Ruins/Dodoist").foundDodo5 = true
	get_node("/root/Main/Ruins/Dodoist").updateNumOfDodos()
	isCatched = true
	#$AnimatedSprite.play()
	#translation_speed = 3
	
func leaveDodoAlone() :
	$AnimatedSprite.play()
	translation_speed = 3	
