extends Node2D

var dodoAppeared = false

func selectDialogs() :
	if($Quest.is_accepted() && !dodoAppeared):
		$QuestDialog.show()
	elif dodoAppeared:
		$QuestClearDialog.show()
	else :
		$Dialog.show()
		
		
func selectBushDialog() :
	
	if Global.find_player().has_follower(Global.AuraType.RUINS) && !dodoAppeared:
		$BushDialogDodo.show()
		activateDodo()
		$DodoBush.visible = true
		$BushWiggle/AnimatedSprite.stop()
		dodoAppeared = true
	elif (dodoAppeared):
		$NothingDialog.show()	
	
	else :
		$BushDialog.show()
			
			
func activateDodo() :
	$DodoBush.isActive = true			

func questClear() :
	$Quest.set_description("It was only a Dodo in the bush.") 

